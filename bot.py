from slackclient import SlackClient
import market
import esi
import numpy as np
from slack import Slackbot

import logging
import sys

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# create a file handler
# handler = logging.FileHandler('debug.log')
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)

# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)

from config import SLACK_BOT_TOKEN

slack = SlackClient(SLACK_BOT_TOKEN)
bot = Slackbot(slack_client=slack, logger=logger)

hubs = [
    {'name': 'Amarr', 'region_id': 10000043, 'station_id': 60008494},
    {'name': 'Jita', 'region_id': 10000002, 'station_id': 60003760},
    {'name': 'Dodixie', 'region_id': 10000032, 'station_id': 60011866}
]


@bot.command('help', help='Shows list of supported commands.')
def help(channel, arg):
    if arg is None:
        message = 'Supported commands: *{}*.\n*help _command_* for detailed help.'.format(
            ', '.join(bot._commands.keys()))
    elif arg in bot._command_help:
        message = bot._command_help[arg]
    else:
        message = 'No help entry found for {}'.format(arg)

    return bot.post_message(channel, message)


@bot.command('ping', help='Check bot presence.')
def ping(channel, arg):
    if arg:
        return bot.post_message(channel, arg)
    return bot.post_message(channel, 'Pong')


@bot.command('price', help='Get lowest sell price in Jita 4-4')
def price(channel, arg):
    bot.set_typing(channel)
    if arg:
        types = []
        for type in arg.splitlines():
            if len(type) < 3:
                continue
            types.extend(esi.search_type(type))
        if len(types) > 50:
            message = 'Too many possible matches ({}). Aborting.'.format(len(types))
        elif len(types) > 0:
            names = esi.names(types)
            names = list(filter(lambda x: filter_types(arg, x['name']), names))
            prices = market.min_sell_for_types(names)
            items = '\n'.join(['{}:\t{:0,.2f} ISK'.format(name, price) for name, price in prices])
            total = '\n\nSet total: *{:0,.2f} ISK*'.format(np.sum(price for name, price in prices))
            message = '*Lowest sell orders in Jita 4-4:*\n{}{}'.format(items, total if len(prices) > 1 else '')
        else:
            message = 'No results found'
    else:
        message = 'No query supplied. Usage: *price _item(s)_*'

    return bot.post_message(channel, message)


@bot.command('pricehub', help='Get Prices in market hubs')
def pricehub(channel, arg):
    bot.set_typing(channel)
    if arg:
        types = []

        for type in arg.splitlines():
            if len(type) < 3:
                continue
            types.extend(esi.search_type(type) or [])
        if len(types) > 50:
            message = 'Too many possible matches ({}). Aborting.'.format(len(types))
        elif len(types) > 0:
            names = esi.names(types)
            names = list(filter(lambda x: filter_types(arg.lower(), x['name']), names))
            #TODO: Refactor. This is horrifying.
            totals = []
            for hub in hubs:
                prices = market.min_sell_for_types(names, hub['region_id'], hub['station_id'])
                totals.append('{}: *{:,.2f} ISK* ({}/{} items)'.format(
                    hub['name'],
                    np.sum(price for name, price in prices),
                    len(prices),
                    len(names)
                ))
            message = '{}\n_______________________\n{}'.format('\n'.join(set(name['name'] for name in names)), '\n'.join(totals) if len(totals) > 0 else 'No orders found')
        else:
            message = 'No results found'
    else:
        message = 'No query supplied. Usage: *pricehub _item(s)_*'

    return bot.post_message(channel, message)


def filter_types(search, name):
    blacklist = ['Blueprint', 'SKIN', 'Issue']
    for term in blacklist:
        if term.lower() in name.lower() and not term.lower() in search.lower():
            return False

    return True

if __name__ == '__main__':
    bot.run()
