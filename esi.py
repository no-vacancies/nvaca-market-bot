import json

from esipy import App, EsiClient
from esipy.cache import FileCache

import config

esiapp = App.create(config.ESI_SWAGGER_JSON)

esiclient = EsiClient(
    # cache=RedisCache(Redis(config.REDIS_CACHE_HOST, port=config.REDIS_CACHE_PORT)),
    cache=FileCache('.webcache'),
    headers={'User-Agent': config.ESI_USER_AGENT}
)

from bot import logger


def search_type(name):
    """
    Search item types
    :param name: Search string
    :return: List of matching typeIDs
    """
    op = esiapp.op['get_search'](categories=['inventory_type'], search=name)

    result = esiclient.request(op, raw_body_only=True)
    return _check_result(result).get('inventory_type', [])


def names(type_list):
    """
    Get full name for a list of typeIDs
    :param type_list: List of valid typeIDs
    :return: List of dicts containing typeID, Name and group
    """
    op = esiapp.op['post_universe_names'](ids=type_list)

    result = esiclient.request(op, raw_body_only=True)

    return _check_result(result)


def get_sell_orders(types, region_id=10000002):
    """
    Get all sell orders for list of types in a given region
    :param types: List of valid typeIDs
    :param region_id: region ID to be checked
    :return: List of dicts containing market info.
    """
    ops = [esiapp.op['get_markets_region_id_orders'](region_id=region_id, type_id=t, order_type='sell') for t in types]

    results = esiclient.multi_request(ops, threads=30, raw_body_only=True)
    results_json = []
    for request, result in results:
        results_json.append(_check_result(result))
    return results_json


def _check_result(result):
    """
    Check if an ESI result is valid
    :param result: PySwagger result object from esipy
    :return: Json parsed to dict.
    """
    dict = json.loads(result.raw.decode('utf-8'))
    if result.status == 200:
        return dict
    else:
        logger.error("{}: {}".format(result.status, dict.get('error')))
        raise ValueError(dict.get('error', ''))